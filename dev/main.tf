#module "network" {
#  source = "git@bitbucket.org:srijwala_n/gke-terraform.git//tfm/network?ref=master"
#  // base network parameters
#  region      = "us-east1"
#  environment = "dev"
#  provision_vpc = true
#  network_name = "default"
#  fqdn = "gke-dev.test.com"
#  project_id = "devops-non-prod"
#}

module "cluster" {
  source     = "git@bitbucket.org:srijwala_n/gke-terraform.git//tfm/cluster?ref=master"
  region     = "us-east1"
  project_id = "devops-non-prod"
  environment = "dev"
  cluster = "gke-dev"
  dns_region = "useast1"
#  network_name = "default"

}

module "node_pool" {
  source             = "git@bitbucket.org:srijwala_n/gke-terraform.git//tfm/node_pool?ref=master"
  region             = "us-east1"
  dns_region         = "us-east1"
  machine_type       = "n1-standard-4"
  project_id         = "devops-non-prod"
  cluster            = "gke-dev"
  # min_node_count     = "1"
  # max_node_count     = "2"
}

terraform {
  backend "gcs" {
    bucket  = "tf-gke-statefiles"
    prefix  = "gke-dev"
  }
}

#######################
# Create the network and subnetworks, including secondary IP ranges on subnetworks
#######################
resource "google_compute_network" "vpc" {
    count = var.provision_vpc == true ? 1 : 0
    name = var.network_name
    auto_create_subnetworks = "true"
    routing_mode = "GLOBAL"
    project = var.project_id
    description = var.description
  
}

# DNS zone creation
# resource "google_dns_managed_zone" "dns-zone" {
#     project = var.project_id
#     name = "base-dns-global-${var.environment}"
#     dns_name = var.fqdn
#     description = "base-dns-global-${var.environment}"
#     visibility = "public"

#     dnssec_config {
#         state = "off"
#     }

#     depends_on = [
#       google_compute_network.vpc[0]
#     ]
  
# }






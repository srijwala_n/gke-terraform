#######################
# Define all the variables we'll need
#######################
variable "project_id" {
    type = string
    description = "The ID of the project where this VPC will be created"
  
}

variable "description" {
    type = string
    description = "VPC description"
    default = ""
}

variable "environment" {
    type = string
    description = "The environment of the project where this VPC will be created"
}

variable "network_name" {
    type = string
    description = "VPC name"
}

variable "provision_vpc" {
    type = bool
    description = "Flag for creating a VPC network"
    default = false
}

variable "region" {
    type = string
    description = "Region where the router resides"
}

# variable "fqdn" {
#     type = string
#     description = "FQDN for the VPC"
# }
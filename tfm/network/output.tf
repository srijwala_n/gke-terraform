# /** provide outputs to be used in GKE cluster creation **/
# #VPC outputs
# output "network" {
#     value = google_compute_network.vpc[0]
#     description = "The VPC resource being created"
# }

# output "network_name" {
#     value = "google_compute_network.vpc[0].name"
#     description = "The name of the VPC being created"
# }

# output "network_id" {
#     value = google_compute_network.vpc[0].id
#     description = "The ID of the VPC being created"
# }

# output "network_self_link" {
#     value = google_compute_network.vpc[0].self_link
#     description = "The URI of the VPC being created"
# }

# #DNS Zone outputs
# output "name" {
#     value = google_dns_managed_zone.dns-zone.name
#     description = "The DNS Zone name"
# }

# output "type" {
#     value = google_dns_managed_zone.dns-zone.visibility
#     description = "The DNS zone type"
# }

# output "domain" {
#     value = google_dns_managed_zone.dns-zone.dns_name
#     description = "The DNS Zone domain"
# }

# output "name_servers" {
#     value = google_dns_managed_zone.dns-zone.name_servers
#     description = "The DNS Zone name servers"
# }

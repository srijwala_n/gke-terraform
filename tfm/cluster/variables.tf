variable "environment" {
  description = "Environment"
  type = string
}

variable "region" {
  type = string
  description = "The region where the GKE cluster will be created."
}

variable "project_id" {
  description = "The project where the GKE cluster will be created. Leave unspecified to use the project from the provider."
  type = string
}

variable "dns_region" {
  type = string
  description = "dns region"
}

variable "cluster" {
  type = string
  description = "cluster name"
}

variable "network_name" {
  type = string
  description = "the network name of the gke cluster"
}

variable "enable_private_endpoint" {
  description = "A boolean to enable private (non public) kube-api endpoints"
  default     = false
}

variable "enable_private_nodes" {
  description = "A boolean to enable private (non public) nodes"
  default     = false
}

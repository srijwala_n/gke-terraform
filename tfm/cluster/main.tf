# data "google_compute_network" "vpc1" {
#   name = var.environment
#   project = var.project_id
# }


resource "google_container_cluster" "cluster" {
  name        = var.cluster
  project     = var.project_id
  description = "Demo GKE Cluster"
  location    = var.region
#  network     = data.google_compute_network.vpc1.name
#  subnetwork  = var.gke_subnetwork
#  logging_service    = var.gke_logging_service
#  monitoring_service = var.gke_monitoring_service
  initial_node_count = 1
  remove_default_node_pool = true
  
  # ip_allocation_policy {
  #   cluster_secondary_range_name  = var.ip_range_pods
  #   services_secondary_range_name = var.ip_range_services
  # }

  # cluster_autoscaling {
  #   enabled = true
  #   resource_limits {
  #     resource_type = "cpu"
  #     minimum = 1
  #     maximum = 4
  #   }
  #   resource_limits {
  #     resource_type = "memory"
  #     minimum = 4
  #     maximum = 16
  #   }
  # }
  addons_config {
  http_load_balancing {
    disabled = false
  }
  
  horizontal_pod_autoscaling {
    disabled = false
  }
} 
  master_auth {
    
     client_certificate_config {
       issue_client_certificate = true
     }
  }
}


variable "region" {
  type = string
  description = "The region where the GKE cluster will be created."
}

variable "project_id" {
  description = "The project where the GKE cluster will be created. Leave unspecified to use the project from the provider."
  type = string
}

variable "dns_region" {
  type = string
  description = "dns region"
}

variable "cluster" {
  type = string
  description = "cluster name"
}

variable "initial_node_count" {
  description = "The initial node count for the pool, per availability zone. Changing this will force recreation of the resource."
  default     = "1"
}


variable "image_type" {
  description = "The OS image to be used for the nodes."
  default     = "COS"
}

variable "machine_type" {
  description = "The machine type of nodes in the pool."
  default     = "n1-standard-4"
}

variable "disk_size_in_gb" {
  description = "Disk size, in GB, for the nodes in the pool."
  default     = "100"
}


variable "disk_type" {
  description = "Type of the disk attached to each node"
  default     = "pd-standard"
}

# variable "additional_oauth_scopes" {
#   type        = list
#   description = "List of strings for additional oauth scope in a node config"
#   default     = []
# }

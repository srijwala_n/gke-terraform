resource "google_container_node_pool" "default" {
  name       = var.cluster
  project    = var.project_id
  location   = var.region
  cluster    = var.cluster
  node_count = 1

  node_config {
    preemptible               = false
    machine_type              = var.machine_type
    local_ssd_count           = 0
    disk_size_gb              = 100
    disk_type                 = "pd-standard"
    image_type                = "COS"
    
    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}